#!/usr/local/bin/python3
import subprocess

import boto3
import hashlib
import os
import shutil
import tarfile
import tempfile
from pathlib import Path

TOOL_DIR = Path(os.path.realpath(__file__)).parent
PROJECT_ROOT = TOOL_DIR / '..' / 'gi-client'
kbuildDir = PROJECT_ROOT / 'build' / 'libs'

gradleBuild = PROJECT_ROOT / 'build.gradle'
version = ''
for line in gradleBuild.open().readlines():
    if line.startswith('version'):
        version = line.split("=")[1].strip().replace("'", "")
        break

tarName = 'gi-{0}.tar.gz'.format(version)
tarFile = Path.home() / tarName
tarPath = str(tarFile)


def replace_tokens(in_, out_, shasum=''):
    for line in in_.readlines():
        output = line.replace('***VERSION', version).replace('***SHASUM', shasum)
        out_.write(output)


with tempfile.TemporaryDirectory() as tmp:

    jarName = 'gi-{0}.jar'.format(version)
    jar = kbuildDir / jarName
    shutil.copy(str(jar), tmp)
    print('Jar copied to temp dir')

    os.chdir(str(TOOL_DIR))
    shutil.copy('gi-complete.bash', tmp)
    print('bash completion script copied to temp dir')
    docDir = Path(tmp) / 'doc'
    docDir.mkdir()
    subprocess.run(['ronn', '-r', 'gi.1.md'])
    shutil.move('gi.1', str(docDir))
    print('Man page moved to doc dir')

    binDir = Path(tmp) / 'bin'
    binDir.mkdir()
    binary = binDir / 'gi'
    with (TOOL_DIR / 'gi').open() as executable, \
            binary.open('w') as dest:
        replace_tokens(executable, dest)
    binary.chmod(0o755)
    print('gi binary copied to temp dir')

    os.chdir(tmp)
    with tarfile.open(tarPath, 'w:gz') as tar:
        for name in [jarName, 'gi-complete.bash', 'doc', 'bin']:
            tar.add(name)
    os.chdir(str(Path.home()))
    print('tarfile written to {0}'.format(tarPath))

hasher = hashlib.sha256()
hasher.update(tarFile.read_bytes())
shasum = hasher.hexdigest()
print('SHA-256 hash calculated: {0}'.format(shasum))

BREW_FILE_REPO = Path.home() / 'git-repos' / 'homebrew-gi'

with (BREW_FILE_REPO / 'gi.rb').open('w') as brewFile, \
        (TOOL_DIR / 'gi-homebrew-template.txt').open() as template:
    replace_tokens(template, brewFile, shasum)

print("Brewfile updated in brewfile repo (don't forget to commit and push)")

print('Starting upload to S3...')

BUCKET_NAME = 'org.tkeating.gi'

s3 = boto3.resource('s3')
bucket = s3.Bucket(BUCKET_NAME)

with open(tarPath, 'rb') as uploadTar:
    bucket.put_object(Key=tarName, Body=uploadTar)

acl = s3.ObjectAcl(BUCKET_NAME, tarName)
acl.put(ACL='public-read')

print('Tarfile uploaded to S3')


