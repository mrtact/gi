require "formula"

class Gi < Formula
  VERSION = "***VERSION"

  desc "Alternate command line client for git"
  homepage "https://gitlab.com/mrtact/gi"
  url "https://s3-us-west-2.amazonaws.com/org.tkeating.gi/gi-#{VERSION}.tar.gz"
  sha256 "***SHASUM"

  bottle :unneeded

  def install
    inreplace "bin/gi", "GI_LIBEXEC", "#{libexec}"
  	libexec.install Dir["*"]
    bin.install_symlink "#{libexec}/bin/gi" => "gi"
  	man1.install_symlink "#{libexec}/doc/gi.1"
  	ohai "Bash completion script installed to #{libexec}/gi-complete.bash. Source this file to enable completion."
  end
end
