
# gi(1) — an alternate command line UI for git
## SYNOPSIS
`gi` verb [<direct object>]

## DESCRIPTION
`gi` is an alternative command-line git client. It provides a more unified _natural-language syntax_, with _fuzzy matching_.

## NATURAL LANGUAGE
The structure of a `gi` command is similar to the sentence structure of most western languages:
_verb_ _direct-object_, where _verb_ is the `gi` command you wish to invoke and _direct-object_ is its target. The object is frequently optional — for example, the _pull_ verb does not require a DO, and some verbs have a default DO, so if the DO is omitted, the command will attempt to act on its default.

Some commands also take one or more additional arguments.

## FUZZY MATCHING
In the spirit of making a modest effort toward natural language processing, `gi` will attempt to fuzzy-match any known tokens (verbs or DOs). This means you only need to supply enough characters for the matcher to uniquely identify what token you are trying to invoke. You can abbreviate `gi list branches` as `gi ls br`. In the event that an abbreviated argument matches more than one token equally, the command will fail with a warning.
If you have concerns about whether a particular short version of a command will match correctly, you can test it against the matcher using the command `gi fuzztest`.

## DIRECT OBJECTS
The list of supported target entities includes:

  * `branches`:
    git branches.
  * `changes`:
    Current uncommitted changes.
  * `commits`:
    Committed changes.
  * `remotes`:
    Remote repos.
  * `tags`:
    git tags

## VERBS
  * `commit` [<Optional commit message>]:
    Commit the currently staged changes. If you don't supply a commit message, `gi` will invoke your preferred editor, specified exactly as for the existing git command line tool.
  * `create` <DO> _name_:
    Create a new object of <DO> type. Valid DOs: branch, remote, tag
  * `fuzztest` <string>:
    Shows a list of tokens that match the provided string
  * `list` <DO>:
    List objects of <DO> type. Valid DOs: branches, changes, commits, remotes, stashes, users, tags
    Default DO: `changes`
  * `remove` <DO> <name>:
    Remove an object of <DO> type. Valid DOs: branches, remotes, stashes, tags
  * `stage` [<names>...]:
    Add named objects to the stage. In the absence of any arguments, stages all current unstaged changes.
  * `switch` <branch name>:
    Switches to the named branch
  * `unstage` [<names>...]:
    Removes named objects from the stage. In the absence of any arguments, unstages everything currently in the stage.
