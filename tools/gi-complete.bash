#!/usr/bin/env bash
_gi_completions() {
	COMPREPLY=()
	result=$(gi complete ${COMP_LINE});
	while read -r line; do
		COMPREPLY+=("$line")
	done <<< "$result"
}

complete -F _gi_completions gi;
