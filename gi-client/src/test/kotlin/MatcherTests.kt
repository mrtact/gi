package org.tkeating.gi

import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.tkeating.gi.parser.*


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MatcherTests {
    private var testTokens: List<String> = mutableListOf()

//    @BeforeAll
//    fun setup() {
//        testTokens = ('a'..'z').map { first ->
//            ('a'..'z').map { second ->
//                "$first$second"
//            }
//        }.flatten()
//    }

    @Test
    @DisplayName("A complete token should match itself")
    fun matchSpecificToken() {
        val candidates = listOf("stage", "nuke", "command")
        assertEquals("command", fuzzyMatchToken("command", candidates))
    }

//    @Test
//    @Disabled
//    @DisplayName("Two character combos should resolve to expected counts of commands")
//    fun twoCharacterMatches() {
//        // TODO: Using MutableList<String> blows up compilation (receiver mismatch) on + token
//        val result = mutableMapOf<String, List<String>>().withDefault { listOf() }
//
//        // This is arguably a bastardization of the use of exceptions.
//        // TODO: Once the unfuzzer returns an object, consider using error objects instead.
//        testTokens.forEach { token ->
//            val key = try {
////                fuzzyMatchToken(token, verbs)
//            } catch (e: NotFoundException) {
//                "None"
//            } catch (e: AmbiguousResultException) {
//                "FailDisambiguate"
//            }
//
//            result[key] = result.getValue(key) + token
//        }
//
////        result.entries.forEach { println("${it.key}:\n${it.value}\n${it.value.size}") }
//
//        assertEquals(589, result["None"]?.size)
//        assertEquals(8, result["track"]?.size)
//        assertEquals(5, result["stage"]?.size)
//        assertEquals(4, result["stash"]?.size)
//        assertEquals(3, result["rebase"]?.size)
//        assertEquals(8, result["fetch"]?.size)
//        assertEquals(7, result["merge"]?.size)
//        assertEquals(4, result["remove"]?.size)
//        assertEquals(5, result["view"]?.size)
//        assertEquals(6, result["info"]?.size)
//        assertEquals(7, result["FailDisambiguate"]?.size)
//        assertEquals(5, result["show"]?.size)
//        assertEquals(4, result["list"]?.size)
//        assertEquals(3, result["switch"]?.size)
//        assertEquals(6, result["nuke"]?.size)
//        assertEquals(4, result["pull"]?.size)
//        assertEquals(1, result["unnuke"]?.size)
//        assertEquals(1, result["unstage"]?.size)
//        assertEquals(5, result["changes"]?.size)
//    }

//    @Test
//    @Disabled
//    fun testTokenMatch() {
//        listOf("cm", "ci", "ct", "cts", "com")
//                .forEach {
//                    println("$it ${fuzzyMatchToken(it, tokens)}")
//                }
//    }

    @Test
    @DisplayName("Offset matching should provide expected match scores")
    fun testOffsetMatching() {
        assertEquals(1344, measureOffset("fzt", "fuzztest"))
        assertEquals(1280, measureOffset("fzt", "fetch"))
    }


    @Test
    @DisplayName("Grouped matcher should return appropriate groups")
    fun testGroupedMatch() {
        val candidates = listOf("unnuke", "unstash", "unstage", "thunder", "upnose")
        val result = fuzzyMatcher.groupedMatch("un", candidates)

        assertEquals(2, result.size)
        // Directly matching the substr should score a 90
        assertNotNull(result[90])
        assertEquals(4, result[90]?.size)
    }

    @Test
    @DisplayName("Fuzzy match should throw AmbiguousResultException on multiple matches")
    fun testMultipleFuzzyMatch() {
        val candidates = listOf("unnuke", "unstash", "unstage", "thunder", "upnose")

        assertThrows(AmbiguousResultException::class.java,
                { fuzzyMatchToken("un", candidates) })
    }

    @Test
    @DisplayName("Fuzzy match should throw NotFoundException on not found")
    fun testNotFoundFuzzyMatch() {

        val candidates = listOf("stage", "list", "commit", "track")

        assertThrows(NotFoundException::class.java,
                { fuzzyMatchToken("un", candidates) })
    }
}
