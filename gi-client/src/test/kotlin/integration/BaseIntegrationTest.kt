import com.jcraft.jsch.JSch
import com.jcraft.jsch.Session
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.transport.JschConfigSessionFactory
import org.eclipse.jgit.transport.OpenSshConfig.Host
import org.eclipse.jgit.transport.SshSessionFactory
import org.eclipse.jgit.transport.SshTransport
import org.eclipse.jgit.util.FS
import org.junit.jupiter.api.TestInstance
import org.tkeating.gi.Gi
import java.io.File
import java.nio.file.Files


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class BaseIntegrationTest() {
    val repoUri = """git@github.com:MrTact/git-test.git"""
    val repoDir: File = Files.createTempDirectory("gi-test-repo").toFile()
    var sshSessionFactory: SshSessionFactory

    init {
        repoDir.deleteOnExit()

        val sshKey = ClassLoader.getSystemResource("git-test-key")

        sshSessionFactory = object : JschConfigSessionFactory() {
            override fun configure(host: Host, session: Session) {
                session.setConfig("StrictHostKeyChecking", "no")
            }

            override fun createDefaultJSch(fs: FS?): JSch {
                val defaultJSch = JSch()
                defaultJSch.addIdentity(sshKey.path)
                return defaultJSch
            }
        }

        Gi.getGit = ::initGit

        // We do this because instantiation of the git repo (which is a clone in this case
        // is lazy. Some tests put a file in the directory before performing any git
        // operations, and this will cause the clone op to fail
        Gi.git.status().call()
    }

    fun initGit() = Git.cloneRepository()
            .setURI(repoUri)
            .setDirectory(repoDir)
            .setTransportConfigCallback { transport ->
                val sshTransport = transport as SshTransport
                sshTransport.sshSessionFactory = sshSessionFactory
            }
            .call()
}
