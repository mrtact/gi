import org.assertj.core.api.KotlinAssertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.tkeating.gi.Gi
import org.tkeating.gi.commands.StageCommand
import org.tkeating.gi.parser.Params
import org.tkeating.gi.runMain
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

const val TEST_FILE_NAME = "testFile.txt"
const val EXISTING_FILE_NAME = "README.md"

class StageIT : BaseIntegrationTest() {
    @Test
    @DisplayName("Staging an untracked file should add it to the index")
    fun stageAdded() {
        val testFile = ClassLoader.getSystemResourceAsStream(TEST_FILE_NAME)
        val testPath = listOf(repoDir.toString(), TEST_FILE_NAME).joinToString(File.separator)
        Files.copy(testFile, Paths.get(testPath))

        var status = Gi.git.status().call()
        assertThat(status.untracked).hasSize(1).contains(TEST_FILE_NAME)

        runMain(listOf("sg"))

        status = Gi.git.status().call()
        assertThat(status.untracked).isEmpty()
        assertThat(status.added).hasSize(1).contains(TEST_FILE_NAME)

        Gi.git.rm().addFilepattern(testPath).call()
        Files.delete(Paths.get(testPath))
    }

    @Test
    @DisplayName("Staging a modified file should add it to the index")
    fun stageModified() {
        val testFile = ClassLoader.getSystemResourceAsStream(TEST_FILE_NAME)
        val testPath = listOf(repoDir.toString(), EXISTING_FILE_NAME).joinToString(File.separator)
        Files.copy(testFile, Paths.get(testPath), StandardCopyOption.REPLACE_EXISTING)

        var status = Gi.git.status().call()
        assertThat(status.modified).hasSize(1).contains(EXISTING_FILE_NAME)

        runMain(listOf("sg"))

        status = Gi.git.status().call()
        assertThat(status.modified).isEmpty()
        assertThat(status.changed).hasSize(1).contains(EXISTING_FILE_NAME)

        Gi.git.reset().addPath(testPath).call()
        Gi.git.checkout().addPath(testPath).call()
    }

    @Test
    @DisplayName("Staging a missing file should show up as a remove in the index")
    fun stageMissing() {
        val testPath = listOf(repoDir.toString(), EXISTING_FILE_NAME).joinToString(File.separator)
        Files.delete(Paths.get(testPath))

        var status = Gi.git.status().call()
        assertThat(status.missing).hasSize(1).contains(EXISTING_FILE_NAME)

        runMain(listOf("sg"))

        status = Gi.git.status().call()
        assertThat(status.missing).isEmpty()
        assertThat(status.removed).hasSize(1).contains(EXISTING_FILE_NAME)

        Gi.git.reset().addPath(testPath).call()
        Gi.git.checkout().addPath(testPath).call()
    }

    @Test
    @DisplayName("Unstaging an added file should show as untracked")
    fun unstageAdded() {
        val testFile = ClassLoader.getSystemResourceAsStream(TEST_FILE_NAME)
        val testPath = listOf(repoDir.toString(), TEST_FILE_NAME).joinToString(File.separator)
        Files.copy(testFile, Paths.get(testPath))

        StageCommand().execute()

        var status = Gi.git.status().call()
        assertThat(status.added).hasSize(1).contains(TEST_FILE_NAME)

        runMain(listOf("unst"))

        status = Gi.git.status().call()
        assertThat(status.added).isEmpty()
        assertThat(status.untracked).hasSize(1).contains(TEST_FILE_NAME)

        Gi.git.rm().addFilepattern(testPath).call()
        Files.delete(Paths.get(testPath))
    }

    @Test
    @DisplayName("Unstaging a changed file should show as modified")
    fun unstageChanged() {
        val testFile = ClassLoader.getSystemResourceAsStream(TEST_FILE_NAME)
        val testPath = listOf(repoDir.toString(), EXISTING_FILE_NAME).joinToString(File.separator)
        Files.copy(testFile, Paths.get(testPath), StandardCopyOption.REPLACE_EXISTING)

        StageCommand().execute()

        var status = Gi.git.status().call()
        assertThat(status.changed).hasSize(1).contains(EXISTING_FILE_NAME)

        runMain(listOf("unst"))

        status = Gi.git.status().call()
        assertThat(status.changed).isEmpty()
        assertThat(status.modified).hasSize(1).contains(EXISTING_FILE_NAME)

        Gi.git.reset().addPath(testPath).call()
        Gi.git.checkout().addPath(testPath).call()
    }

    @Test
    @DisplayName("Unstaging a removed file should show up as missing")
    fun unstageMissing() {
        val testPath = listOf(repoDir.toString(), EXISTING_FILE_NAME).joinToString(File.separator)
        Files.delete(Paths.get(testPath))

        StageCommand().execute()

        var status = Gi.git.status().call()
        assertThat(status.removed).hasSize(1).contains(EXISTING_FILE_NAME)

        runMain(listOf("unst"))

        status = Gi.git.status().call()
        assertThat(status.removed).isEmpty()
        assertThat(status.missing).hasSize(1).contains(EXISTING_FILE_NAME)

        Gi.git.reset().addPath(testPath).call()
        Gi.git.checkout().addPath(testPath).call()
    }
}
