import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.tkeating.gi.git.GitConfig

class ConfigTests : BaseTest() {
    @Test
    @DisplayName("Global config should override local")
    fun getConfig() {
        val editor = GitConfig.editor
        println(editor)
    }
}
