import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.tkeating.gi.commands.FuzzTestCommand
import org.tkeating.gi.parser.Params
import org.tkeating.gi.runMain
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.util.*

class CommandTests : BaseTest() {
    @Test
    @DisplayName("List with no args should list changes")
    fun listChanges() {
        runMain(listOf("ls"))
        println("\nTest complete")
    }

    @Test
    @DisplayName("Listing tags should list tags")
    fun listTags() {
        runMain(listOf("ls", "tags"))
        println("\nTest complete")
    }

    @Test
    @DisplayName("Listing stashes should list stashes")
    fun listStashes() {
        runMain(listOf("ls", "stashes"))
        println("\nTest complete")
    }

    @Test
    @DisplayName("List remotes")
    fun listRemotes() {
        runMain(listOf("ls", "rem"))
        println("\nTest complete")
    }

    @Test
    @DisplayName("List branches")
    fun listBranches() {
        runMain(listOf("ls", "br"))
        println("\nTest complete")
    }

    @Test
    @DisplayName("Fuzztest command should return expected matches")
    fun fuzzTestMatch() {
        val byteStream = ByteArrayOutputStream()
        System.setOut(PrintStream(byteStream))
        FuzzTestCommand(listOf("fzt")).execute()
        assertEquals("[fuzztest]\n", byteStream.toString())
    }

    @Test
    @DisplayName("Create a branch")
    fun branchCreateTest() {
        val name = UUID.randomUUID().toString()
        val args = listOf("create", "branch", name)
        runMain(args)
        println("\nTest complete")
    }

    @Test
    @DisplayName("Create a remote")
    fun createRemoteTest() {
        val name = UUID.randomUUID().toString()
        val args = listOf("create", "remote", name, "git@gitlab.com:mrtact/gi.git")
        runMain(args)
        println("\nTest complete")
    }

    @Test
    @DisplayName("Remove a tag")
    fun removeTagTest() {
        val tag1 = UUID.randomUUID().toString()
        val tag2 = UUID.randomUUID().toString()
        execute("create tag $tag1")
        execute("create tag $tag2")
        execute("rm tag $tag1 $tag2")
    }

    @Test
    @DisplayName("Remove a remote")
    fun removeRemoteTest() {
        val remote = UUID.randomUUID().toString()
        execute("cr rm $remote git:someURI")
        execute("rm remote $remote")
    }

    @Test
    @DisplayName("Commit with commit message")
    fun commitWithCommitMessage() {
        execute("ci And this is my commit message")
    }

    @Test
    @DisplayName("Switch to nonexistent branch should create it")
    fun switchBadBranch() {
        execute("switch foo")
    }

    @Test
    @DisplayName("Trying to stage a file when there are two with the same name should fail")
    fun stageDupeFile() {
        execute("stage Stage.kt")
    }

    @Test
    @DisplayName("Staging a directory should stage all files under that directory")
    fun stageDirectory() {
        execute("stage src/main/kotlin/org/tkeating/gi")
    }

    @Test
    @DisplayName("Unstaging a directory should stage all files under that directory")
    fun unstageDirectory() {
        execute("unstage src/main/kotlin/org/tkeating/gi")
    }

    @Test
    @DisplayName("Pull should pull changes, stashing as needed")
    fun pull() {
        execute("pull")
    }

    @Test
    @DisplayName( "blah")
    fun subclassTest() {
        System.setProperty("user.dir", "/Users/tkeating/git-repos/git-test")

        execute("pull")
    }

    @Test
    @DisplayName( "Test completion")
    fun completionTest() {
        execute("complete gi sg ListComma")
    }

    @Test
    @DisplayName("Fuzz test")
    fun fuzzTest() {
        execute("fzt List")
    }
}
