import org.assertj.core.api.KotlinAssertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.tkeating.gi.org.tkeating.gi.extensions.QUOT
import org.tkeating.gi.org.tkeating.gi.parser.tokens.Verb
import org.tkeating.gi.parser.Changes
import org.tkeating.gi.parser.CommandLineGrammar

class GrammarTests {
    lateinit var grammar: CommandLineGrammar

    @BeforeEach
    fun setup() {
        grammar = CommandLineGrammar()
    }

    @Test
    @DisplayName("'fuzztest blah' should parse to verb + arglist")
    fun fuzztestBlah() {
        grammar.parse("fzt qwrty")
        assertThat(grammar.stack)
                .hasSize(2)
                .contains(Verb.fuzztest)
                .contains("qwrty")
    }

    @Test
    @DisplayName("Command with optional param list, should be able to omit the params")
    fun commandWithArgList() {
        grammar.parse("stage")
        assertThat(grammar.stack)
                .hasSize(1)
                .contains(Verb.stage)
    }

    @Test
    @DisplayName("Command with DO should parse to verb + DO + empty arg list")
    fun commandWithDO() {
        grammar.parse("ls ch")

        assertThat(grammar.stack)
                .hasSize(2)
                .contains(Verb.list)
                .contains(Changes)
    }

    @Test
    @DisplayName("Test parsing quoted strings")
    fun quotedString() {
        val quotedString = """"This string, here: contains lots of  weird-ass 'characters'; let's see if it parses""""
        grammar.parse("ci $quotedString")

        assertThat(grammar.stack)
                .hasSize(2)
                .contains(Verb.commit)
                .contains(quotedString.trim(QUOT))
    }

    @Test
    @DisplayName("Basic parse test")
    fun testBasicParse() {
        grammar.parse("ls chg")
        assertThat(grammar.stack)
                .hasSize(2)
    }
}
