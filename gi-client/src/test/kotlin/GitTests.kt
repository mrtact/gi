import org.assertj.core.api.KotlinAssertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.tkeating.gi.Gi
import org.tkeating.gi.initGit
import java.io.File
import java.nio.file.Paths

/**
 * Tests of the Jgit wrapper object
 */
class GitTests : BaseTest() {
    lateinit var expected: File

    @BeforeEach
    fun setup() {
        expected = Paths.get(".git").toAbsolutePath().toFile()
    }

    @Test
    @DisplayName("Gi object git dir should be set correctly")
    fun testGitDir() {
        assertThat(Gi.repo.directory).isEqualTo(expected)
    }

    @Test
    @DisplayName("Gi object should contain correct git dir even from a subdir")
    fun testGitSubdir() {
        System.setProperty("user.dir", Paths.get("src").toAbsolutePath().toString())
        assertThat(Gi.repo.directory).isEqualTo(expected)
    }
}
