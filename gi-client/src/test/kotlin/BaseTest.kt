import org.tkeating.gi.Gi
import org.tkeating.gi.initGit
import org.tkeating.gi.runMain

open class BaseTest {
    init {
        Gi.getGit = ::initGit
    }

    fun execute(vararg strings: String) {
        runMain(strings.toList())
    }

    fun execute(string: String) {
        runMain(string.split(Regex.fromLiteral("\\s+")))
    }
}
