package org.tkeating.gi.extensions

import org.eclipse.jgit.api.Status
import org.tkeating.gi.extensions.FileStatus.*
import org.tkeating.gi.extensions.RenderBlock.*

enum class FileStatus {
    ADDED,
    CONFLICTING,
    DELETED,
    MISSING,
    MODIFIED,
    CHANGED,
    UNTRACKED;
}

enum class RenderBlock {
    STAGED,
    UNSTAGED,
    CONFLICTED
}

val stagedBlock = listOf(ADDED, DELETED, CHANGED)
val unstagedBlock = listOf(MISSING, MODIFIED, UNTRACKED)
val conflictedBlock = listOf(CONFLICTING)

fun Status.byStatus(fileStatus: FileStatus) = when (fileStatus) {
    ADDED -> added
    CONFLICTING -> conflicting
    DELETED -> removed
    MISSING -> missing
    MODIFIED -> modified
    CHANGED -> changed
    UNTRACKED -> untracked
}

fun Status.forBlock(block: RenderBlock) = when (block) {
    STAGED -> stagedBlock
    UNSTAGED -> unstagedBlock
    CONFLICTED -> conflictedBlock
}.map { it to byStatus(it) }.toMap()

fun Status.has(statuses: List<FileStatus>) =
        statuses.any { byStatus(it).isNotEmpty() }

fun Status.hasStaged() = this.has(stagedBlock)

fun Status.hasUnstaged() = this.has(unstagedBlock)
