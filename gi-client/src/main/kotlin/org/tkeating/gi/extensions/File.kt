package org.tkeating.gi.extensions

import java.io.File

val workingDir get() = File(System.getProperty("user.dir"))
