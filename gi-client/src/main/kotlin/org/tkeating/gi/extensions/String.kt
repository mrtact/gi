package org.tkeating.gi.org.tkeating.gi.extensions

import org.eclipse.jgit.lib.Repository
import org.tkeating.gi.Gi

val String.isBranch: Boolean
    get() =
        Gi.git.branchList().call().any { Repository.shortenRefName(it.name) == this }

const val QUOT = '"'
