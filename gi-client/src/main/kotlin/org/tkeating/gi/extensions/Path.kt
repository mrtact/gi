package org.tkeating.gi.extensions

import java.io.File
import java.nio.file.Path

data class PathSplit(val dir: String, val file: String)

fun Path.split() = PathSplit(
        file = fileName.toString(),
        dir = take(nameCount - 1).joinToString(File.separator)
)

