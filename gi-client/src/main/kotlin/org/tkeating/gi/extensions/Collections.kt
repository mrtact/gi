package org.tkeating.gi.extensions

import org.tkeating.gi.parser.FuzzyList
import org.tkeating.gi.parser.FuzzyMap
import org.tkeating.gi.parser.Params
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

fun <T> MutableList<T>.append(elem: T) =
        this.apply { add(elem) }

/**
 * Compare a list of elements against a list of types. Returns true if all elements match the
 * specified types.
 *
 * Note: this is very expensive (due to reflection) and we're not using it any longer, but I
 * liked it so I kept it around for future reference.
 */
infix fun <T : KClass<out Any>> List<Any>.like(list: List<T>): Boolean =
        size == list.size &&
                this.asSequence()
                        .zip(list.asSequence())
                        .all {
                            (obj, type_) -> obj::class.isSubclassOf(type_)
                        }

fun List<String>.matchingLastParam(params: List<Any>?) =
        if (params?.isNotEmpty() == true) {
            filter { it.startsWith(params.last().toString()) }
        } else {
            this
        }

fun List<Any>?.toStringList() =
        this?.map(Any::toString) ?: emptyList<String>()

fun Map<String, Any>.toFuzzyMap() = FuzzyMap(this)

fun List<String>.toFuzzyList() = FuzzyList(this)
