package org.tkeating.gi.extensions

public inline fun <T> Iterable<T>.each(action: (T) -> Any): Unit {
    for (element in this) action(element)
}
