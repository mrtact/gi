package org.tkeating.gi.org.tkeating.gi.util

import com.jcraft.jsch.Identity
import com.jcraft.jsch.JSch
import org.tkeating.gi.Gi
import java.time.Instant
import java.util.concurrent.TimeUnit.*
import com.jcraft.jsch.Session
import com.jcraft.jsch.agentproxy.AgentProxyException
import com.jcraft.jsch.agentproxy.Connector
import com.jcraft.jsch.agentproxy.RemoteIdentityRepository
import com.jcraft.jsch.agentproxy.USocketFactory
import com.jcraft.jsch.agentproxy.connector.SSHAgentConnector
import com.jcraft.jsch.agentproxy.usocket.JNAUSocketFactory
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.transport.OpenSshConfig.Host
import org.eclipse.jgit.transport.JschConfigSessionFactory
import org.eclipse.jgit.transport.SshSessionFactory
import org.eclipse.jgit.transport.SshTransport
import org.eclipse.jgit.api.TransportConfigCallback
import org.eclipse.jgit.transport.Transport
import org.eclipse.jgit.util.FS
import org.tkeating.gi.git.GitConfig
import java.io.File




fun countdown(description: String, gerund: String, callback: () -> Unit) {
    println(description)
    print("$gerund in ")
    (5 downTo 1).forEach { print("${it}... "); Thread.sleep(1_000) }
    println()
    callback()
}


fun lastPullStr(): String {
    val stamp = Gi.repo.directory.resolve("FETCH_HEAD").lastModified()
    val ms = Instant.now().toEpochMilli() - stamp
    val formatted = when {
        ms < MINUTES.toMillis(1) -> "just now"
        ms < MINUTES.toMillis(60) -> "${MILLISECONDS.toMinutes(ms)} minutes ago"
        ms < HOURS.toMillis(24) -> "${MILLISECONDS.toHours(ms)} hours ago"
        ms < DAYS.toMillis(30) -> "${MILLISECONDS.toDays(ms)} days ago"
        else -> "More than a month ago"
    }

    val alert = if (ms > HOURS.toMillis(8)) "⚠  " else ""
    return "(${alert}Pulled $formatted)"
}


val sshSessionFactory: SshSessionFactory = object : JschConfigSessionFactory() {
    override fun configure(host: Host, session: Session) {
        // do nothing
    }

/*
    override fun getJSch(hc: Host?, fs: FS?): JSch {
        val jsch = super.getJSch(hc, fs)
        GitConfig.sshKeyName?.let {
            val home = System.getProperty("user.home")
            jsch.addIdentity(listOf(home, ".ssh", it).joinToString(File.separator))

            val id = jsch.identityRepository.identities.lastElement() as Identity
            val success = id.setPassphrase("0comp2u".toByteArray())
        }
        return jsch
    }
*/

    override fun createDefaultJSch(fs: FS?): JSch {
        var con: Connector? = null
        try {
            if (SSHAgentConnector.isConnectorAvailable()) {
                val usf = JNAUSocketFactory()
                con = SSHAgentConnector(usf)
            }
        } catch (e: AgentProxyException) {
            System.out.println(e)
        }

        if (con == null) {
            return super.createDefaultJSch(fs)
        } else {
            JSch.setConfig("PreferredAuthentications", "publickey")
            return JSch().apply() {
                val irepo = RemoteIdentityRepository(con)
                setIdentityRepository(irepo)
                val home = System.getProperty("user.home")
                setKnownHosts(listOf(home, ".ssh", "known_hosts").joinToString(File.separator))
            }
        }
    }
}

fun getTransportConfigCallback() =
        TransportConfigCallback { transport ->
            if (transport is SshTransport) {
                transport.sshSessionFactory = sshSessionFactory
            }
        }
