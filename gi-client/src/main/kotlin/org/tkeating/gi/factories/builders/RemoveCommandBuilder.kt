package org.tkeating.gi.factories.builders

import org.tkeating.gi.commands.CommandWrapper
import org.tkeating.gi.commands.remove.RemoveBranch
import org.tkeating.gi.commands.remove.RemoveRemote
import org.tkeating.gi.commands.remove.RemoveTag
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.Branches
import org.tkeating.gi.parser.DirectObject
import org.tkeating.gi.parser.Remotes
import org.tkeating.gi.parser.Tags

class RemoveCommandBuilder(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = args?.isNotEmpty() == true && args[0] is DirectObject

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() =
            when (args?.get(0)) {
                is Branches -> RemoveBranch(args.drop(1))
                is Remotes -> RemoveRemote(args.drop(1))
                is Tags -> RemoveTag(args.drop(1))
                else -> throw IllegalArgumentException("Invalid args $args")
            }.execute()
}
