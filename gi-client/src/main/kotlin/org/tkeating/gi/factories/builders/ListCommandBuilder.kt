package org.tkeating.gi.factories.builders

import org.tkeating.gi.commands.list.ListBranches
import org.tkeating.gi.commands.list.ListChanges
import org.tkeating.gi.factories.command.*
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.*


class ListCommandBuilder(args: List<Any>?) : CommandChain(args) {
    override val help: String
        get() = "gi list [branches | changes | commits | remotes | stashes | tags | users]"

    override val valid: Boolean
        get() = args?.run { isEmpty() || (isNotEmpty() && get(0) is DirectObject) } ?: true

    override fun execute() =
            when (args?.getOrNull(0)) {
                is Branches -> ListBranches(args.drop(1))
                is Changes, null -> ListChanges(args?.drop(1))
                is Commits -> ListCommits(args.drop(1))
                is Remotes -> ListRemotes(args.drop(1))
                is Stashes -> ListStashes(args.drop(1))
                is Tags -> ListTags(args.drop(1))
                is Users -> ListUsers(args.drop(1))
                else -> throw IllegalArgumentException("Invalid args $args")
            }.execute()
}
