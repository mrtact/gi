package org.tkeating.gi.factories.builders

import org.tkeating.gi.commands.CommandWrapper
import org.tkeating.gi.parser.DirectObject
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

/*
 * Class hierarchy that takes in an (optional) DO instance and returns a callable
 * command wrapper
 */
interface CommandBuilder {
    val name: String
    val defaultDO: DirectObject?
    fun build(DO: DirectObject): CommandWrapper
    fun build(): CommandWrapper = defaultDO?.let(::build)
            ?: throw IllegalArgumentException("No default DO")
}

class SimpleCommandBuilder<T: CommandWrapper>(val klass: KClass<T>) : CommandBuilder {
    override val name = this::class.qualifiedName ?: "SimpleCommandBuilder"

    override val defaultDO = null

    override fun build(DO: DirectObject): CommandWrapper =
            throw IllegalArgumentException("{name} command does not take a DO")

    override fun build() = klass.primaryConstructor!!.call()
}
