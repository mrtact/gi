package org.tkeating.gi.factories.builders

import org.tkeating.gi.commands.create.CreateBranch
import org.tkeating.gi.commands.create.CreateRemote
import org.tkeating.gi.commands.create.CreateTag
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.Branches
import org.tkeating.gi.parser.DirectObject
import org.tkeating.gi.parser.Remotes
import org.tkeating.gi.parser.Tags

class CreateCommandBuilder(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = args?.isNotEmpty() == true && args[0] is DirectObject

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() =
            when (args?.get(0)) {
                is Branches, null -> CreateBranch(args?.drop(1))
                is Remotes -> CreateRemote(args.drop(1))
                is Tags -> CreateTag(args.drop(1))
                else -> throw IllegalArgumentException("Invalid args $args")
            }.execute()
}
