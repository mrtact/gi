package org.tkeating.gi.factories.builders

import org.tkeating.gi.commands.CommandWrapper
import org.tkeating.gi.org.tkeating.gi.extensions.QUOT
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.org.tkeating.gi.parser.tokens.Verb
import org.tkeating.gi.org.tkeating.gi.parser.tokens.verbBuilderFactory
import org.tkeating.gi.parser.CommandLineGrammar
import org.tkeating.gi.parser.DirectObject
import org.tkeating.gi.parser.Params

fun parseCommand(args: List<String>): CommandChain {
    val commandString = args.map {
        if (it.contains("""\s""")) "$QUOT$it$QUOT" else it
    }.joinToString(" ")

    val grammar = CommandLineGrammar()
    grammar.parse(commandString)
    val atoms = grammar.stack.mapNotNull { it }

    return when {
        atoms.isEmpty() -> throw ParseError("No valid gi command found")
        atoms[0] !is Verb ->
            throw ParseError("${atoms[0]} is not recognized as a gi command")
        else -> verbBuilderFactory(atoms[0] as Verb, atoms.drop(1))
    }
}

class ParseError(message: String) : Exception(message)
