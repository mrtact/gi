package org.tkeating.gi.renderers.remotes

import com.andreapivetta.kolor.Color.LIGHT_GRAY
import com.andreapivetta.kolor.Color.WHITE
import org.eclipse.jgit.transport.RemoteConfig

interface RemotesRenderer {
    fun render(remotes: Collection<RemoteConfig>): String
}

object DefaultRemotesTemplate : RemotesRenderer {
    override fun render(remotes: Collection<RemoteConfig>) =
            remotes.map { remote ->
                "${WHITE(remote.name)}\t${LIGHT_GRAY(remote.urIs.first().toASCIIString())}"
            }.joinToString( System.lineSeparator() )
}
