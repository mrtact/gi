package org.tkeating.gi.renderers

import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.lib.Repository

interface RefsRenderer {
    fun render(refs: Collection<Ref>): String
}

object DefaultRefsRenderer : RefsRenderer {
    override fun render(refs: Collection<Ref>) = refs
            .map { Repository.shortenRefName(it.name) }
            .map { "\t$it" }
            .joinToString(System.lineSeparator())
}
