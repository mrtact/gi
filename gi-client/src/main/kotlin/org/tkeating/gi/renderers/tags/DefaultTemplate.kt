package org.tkeating.gi.renderers.tags

import com.andreapivetta.kolor.Color.LIGHT_GRAY
import com.andreapivetta.kolor.Color.WHITE
import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.revwalk.RevWalk
import org.tkeating.gi.Gi

interface TagsRenderer {
    fun render(tags: MutableMap<String, Ref>): String
}

object DefaultTagsTemplate : TagsRenderer {
    override fun render(tags: MutableMap<String, Ref>) = tags.map { (name, ref) ->
        val tag = RevWalk(Gi.repo).parseCommit(ref.objectId)
        val timestamp = tag.authorIdent.`when`
        val info = LIGHT_GRAY("($timestamp by ${tag.authorIdent.name} <${tag.authorIdent.emailAddress}>)")
        val tab = if (name.length >= 16) "\t" else " ".repeat(16 - name.length)
        name to "${WHITE(name)}$tab$info"
    }
            .sortedBy { (name, _) -> name.toLowerCase() }
            .map { (_, text) -> text }
            .joinToString(System.lineSeparator())
}
