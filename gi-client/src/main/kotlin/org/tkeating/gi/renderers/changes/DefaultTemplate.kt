package org.tkeating.gi.renderers.changes

import com.andreapivetta.kolor.Color.*
import com.andreapivetta.kolor.TextEffect.BOLD
import com.andreapivetta.kolor.TextEffect.DIM
import org.eclipse.jgit.api.ListBranchCommand.ListMode.ALL
import org.eclipse.jgit.api.Status
import org.eclipse.jgit.lib.BranchTrackingStatus
import org.eclipse.jgit.lib.Repository
import org.tkeating.gi.Gi
import org.tkeating.gi.extensions.*
import org.tkeating.gi.extensions.FileStatus.*
import org.tkeating.gi.extensions.RenderBlock.*
import org.tkeating.gi.git.relativePath
import org.tkeating.gi.git.remoteInfo
import org.tkeating.gi.org.tkeating.gi.util.lastPullStr
import java.nio.file.Paths

interface ChangesRenderer {
    fun render(status: Status): String
}

val HEADERS = mapOf(
        STAGED to "Changes staged to be committed:",
        UNSTAGED to "Unstaged changes that will not be committed:",
        CONFLICTED to "Conflicting changes:"
)

object DefaultChangesTemplate : ChangesRenderer {
    override fun render(status: Status): String =
            RenderBlock.values()
                    .map { renderBlock(it, status.forBlock(it)) }
                    .filter(String::isNotBlank)
                    .joinToString("\n")
                    .let {
                        "${renderHeader()}\n\n$it"
                    }
}

fun renderHeader(): String {
    val branch = Gi.repo.branch
    val result = mutableListOf<String>()
    result += "\nCurrent branch: ${BOLD(branch)}"

    val tracking = Gi.git.branchList().setListMode(ALL).call()
            .firstOrNull { Repository.shortenRefName(it.name) == branch }
            ?.let { BranchTrackingStatus.of(Gi.repo, it.name) }

    if (tracking != null) {
        result += tracking.remoteInfo()
        result += lastPullStr()
    } else {
        result += "(Not tracking any remote)"
    }

    val joined = result.joinToString(" ")

    return LIGHT_GRAY.BG(BLACK(joined))
}

fun renderBlock(whichBlock: RenderBlock, changes: Map<FileStatus, Set<String>>) =
        renderBlockContent(changes)
                .let { if (it.isNotBlank()) "${HEADERS[whichBlock]}\n$it\n" else "" }

/**
 * Renders changes for a single block (staged, unstaged or conflicting)
 */
fun renderBlockContent(changes: Map<FileStatus, Set<String>>) =
        changes.map { (fs, paths) ->
            paths.map { path -> renderSingleChange(fs, path) }
        }
                .flatMap { it }
                .sortedBy { it.first }
                .map { it.second }
                .joinToString("\n")

/**
 * This returns a Pair<> (filename, rendered line) because we need the raw filename to be able to sort on
 */
fun renderSingleChange(fs: FileStatus, path: String): Pair<String, String> {
    val relPath = Gi.repo.relativePath(workingDir, path)
    val split = Paths.get(relPath).split()
    val name = when (fs) {
        ADDED, UNTRACKED -> LIGHT_GREEN(split.file)
        DELETED, MISSING -> LIGHT_CYAN(split.file)
        MODIFIED, CHANGED -> WHITE(split.file)
        CONFLICTING -> RED(WHITE.BG(split.file))
    }
    val result = split.file.toLowerCase() to "\t$name (${fs.name.toLowerCase()}) ${DIM(split.dir)}"
    return result
}
