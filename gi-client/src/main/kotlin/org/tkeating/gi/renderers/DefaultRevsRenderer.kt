package org.tkeating.gi.renderers

import org.eclipse.jgit.revwalk.RevCommit

interface RevsRenderer {
    fun render(revs: Collection<RevCommit>): String
}

object DefaultRevsRenderer : RevsRenderer {
    override fun render(revs: Collection<RevCommit>) =
            revs.map { rev ->
                "${rev.name}\t${rev.shortMessage}"
            }.joinToString(System.lineSeparator())
}
