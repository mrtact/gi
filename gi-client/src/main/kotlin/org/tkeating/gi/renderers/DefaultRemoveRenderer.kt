package org.tkeating.gi.renderers

import com.andreapivetta.kolor.Color
import com.andreapivetta.kolor.TextEffect
import org.tkeating.gi.parser.Params

class DefaultRemoveRenderer(
        val params: Params,
        val results: Set<String>
) {
    fun render() =
            params.list.map {
                if (it in results) {
                    "${TextEffect.BOLD(it)}: ${Color.GREEN("Removed")}"
                } else {
                    "${TextEffect.BOLD(it)}: ${Color.RED.BG(Color.WHITE("Not found"))}"
                }
            }.joinToString(System.lineSeparator())
}
