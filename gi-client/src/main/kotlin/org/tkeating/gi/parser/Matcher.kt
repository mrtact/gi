package org.tkeating.gi.parser

/**
 * matchFunc: compares two strings and returns a score. The higher the score the better the match
 * filterFunc: permits the matcher to reject entries below a certain score. By default, keeps everything
 */
class Matcher(val matchFunc: (String, String) -> Int,
              val filterFunc: (Pair<Int, String>) -> Boolean = { true }) {

    /**
     * Performs the specified match & filter operations with a string against a list of candidates.
     *
     * @return A LinkedHashMap<Int, List<String>> where
     *      - the Int value is the score returned by calling matchFunc
     *      - the List<> is a list of candidates with that score
     */
    fun groupedMatch(toMatch: String, candidates: List<String>) =
            candidates.map { candidate -> matchFunc(toMatch, candidate) to candidate }
                    .filter { weightedString -> filterFunc(weightedString) }
                    .sortedByDescending { (score, _) -> score }
                    .groupBy({ it.first }, { it.second })

    fun scores(toMatch: String, candidates: List<String>) =
            candidates.map { it to matchFunc(toMatch, it) }
                    .sortedByDescending { it.second }
}

