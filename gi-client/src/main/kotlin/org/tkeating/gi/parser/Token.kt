package org.tkeating.gi.parser

interface Token

class Params(val list: List<String> = emptyList()) : Token {
    companion object {
        val empty = Params()
    }

    operator fun get(index: Int) = list[index]
}
