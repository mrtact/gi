package org.tkeating.gi.parser

import norswap.autumn.Grammar
import norswap.autumn.Parser
import norswap.autumn.parsers.*
import org.tkeating.gi.org.tkeating.gi.extensions.QUOT
import org.tkeating.gi.org.tkeating.gi.parser.FuzzyContainer
import org.tkeating.gi.org.tkeating.gi.parser.tokens.Verb
import org.tkeating.gi.org.tkeating.gi.parser.tokens.verbList
import org.tkeating.gi.org.tkeating.gi.parser.tokens.simpleVerbList

class CommandLineGrammar : Grammar() {
    companion object {
        val parserDebug = System.getenv("PARSER_DEBUG") == "TRUE"
    }
    
    override fun root() = command()

    private fun verb() = debug_parse {
        build_str(
                syntax = { fuzzyWord(verbList) { repeat1 { alpha() } } },
                value = { Verb.valueOf(verbList.fuzzyGet(it.trim())) }
        )
    }

    private fun simpleVerb() = debug_parse {
        build_str(
                syntax = { fuzzyWord(simpleVerbList) { repeat1 { alpha() } } },
                value = { Verb.valueOf(simpleVerbList.fuzzyGet(it.trim())) }
        )
    }

/*
    private fun modifier() = build_str(syntax
            syntax = { word { repeat1 { alpha() }}},
            value = { ParserModifier(it.trim()) }
    )
*/

    private fun directObj() = debug_parse {
        build_str(
                syntax = { fuzzyWord(directObjects) { repeat1 { alpha() } } },
                value = { directObjects[it.trim()]!! }
        )
    }

    private fun quotedString() = debug_parse {
        build_str(
                syntax = { seq { char_set(QUOT) && repeat1 { char_pred { it != QUOT } } && char_set(QUOT) } },
                value = { println(it); it.trim(QUOT) }
        )
    }

    private fun alphaWord() = debug_parse {
        build_str(
                syntax = { repeat1 { char_pred { it != 0.toChar() && !it.isWhitespace() } } }
        )
    }

    private fun argument() = debug_parse {
        word {
            choice { alphaWord() || quotedString() }
        }
    }

    private fun arguments() = repeat0 { argument() }

    private fun simpleCommand() = seq { simpleVerb() && arguments() }

    private fun directObjCommand() = seq { verb() && directObj() && arguments() }

/*
    private fun complexCommand() = seq { verb() && repeat1 { modifier() } && directObj() }
*/

    private fun command() = choice {
        directObjCommand() ||
                simpleCommand() ||
                verb()
    }

    /**
     * Same idea as autumn's built-in [word], except that it interjects a fuzzy match of
     * the returned string using the supplied fuzzy container.
     */
    private fun fuzzyWord(container: FuzzyContainer, p: Parser): Boolean {
        val start = pos
        var success = p()

        if (success) {
            val token = text.substring(start until pos)
            success = container.fuzzyContains(token)
            if (success) {
                parse_whitespace()
            }
        }

        return success
    }

    /**
     * This handles cases where you have tokens that serve similar but different roles,
     * so you want to be able to separate them into different parsers. Does a fuzzy match
     * against ONE set of tokens, but then lets you constrain the match to a specific
     * subset of those tokens, by passing that set in separately.
     */
    private fun fuzzyWordExact(fuzzyMap: FuzzyMap, exactSet: Set<String>, p: Parser): Boolean {
        val start = pos
        var success = p()
        if (success) {
            val token = text.substring(start until pos)
            val matchedKey = fuzzyMap.getKey(token)
            success = exactSet.contains(matchedKey)
            if (success) {
                parse_whitespace()
            }
        }

        return success
    }


    private fun Grammar.debug_parse(p: Parser) =
            if (parserDebug) {
                val start = pos
                val callee = Thread.currentThread().stackTrace[2].methodName
                val result = p()
                val detail =
                        if (result) {
                            val token = text.substring(start until pos).trim()
                            val match = stack.last()
                            "$token -> $match"
                        } else {
                            "no match"
                        }
                println("$callee: $detail")
                result
            } else {
                p()
            }
}

data class ParserModifier(val text: String)
