package org.tkeating.gi.org.tkeating.gi.parser

import org.tkeating.gi.parser.Params

abstract class CommandChain(val args: List<Any>?) {

    abstract val valid: Boolean
    abstract val help: String
    abstract fun execute()
    open fun complete() = listOf("")
}
