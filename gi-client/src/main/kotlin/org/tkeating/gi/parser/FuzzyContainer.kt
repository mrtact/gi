package org.tkeating.gi.org.tkeating.gi.parser

interface FuzzyContainer {
    fun fuzzyContains(element: String): Boolean
}
