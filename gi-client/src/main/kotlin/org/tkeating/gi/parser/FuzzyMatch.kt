package org.tkeating.gi.parser

import me.xdrop.fuzzywuzzy.FuzzySearch

class AmbiguousResultException : Exception()

class NotFoundException : Exception()

/**
 * A matcher that uses Levenshtein score to match, and rejects matches with a score of 50 or less
 */
val fuzzyMatcher = Matcher(
        matchFunc = FuzzySearch::weightedRatio,
        filterFunc = { it.first > 45 })

/**
 * A matcher that attempts to disambiguate the results from fuzzy matching, when there is
 * more than one result with the highest score.
 */
val offsetMatcher = Matcher(matchFunc = ::measureOffset)

/**
 * Takes a string (token) and does fuzzy matching, returning the best match,
 * if there is exactly one. Throws an exception otherwise.
 */
fun fuzzyMatchToken(token: String, candidates: List<String>) =
        with(getFuzzyMatches(token, candidates)) {
            when (size) {
                0 -> throw NotFoundException()
                1 -> first()
                else -> throw AmbiguousResultException()
            }
        }

/**
 * Used for more granular disambiguation than the fuzzy match, this simple hashing function
 * handles input strings up to 10 chars long by scoring more the closer to the start of the
 * string that each character in the substring is found, in sequence.
 */
fun measureOffset(contained: String, containing: String): Int {
    var lastMatchPos = 0

    return contained.map { letter ->
        containing.indexOf(letter, startIndex = lastMatchPos, ignoreCase = true)
                .let { index ->
                    if (index == -1) {
                        0
                    } else {
                        lastMatchPos = index
                        2 pow (10 - index)
                    }
                }
    }.sum()
}

/**
 * Does fuzzy & then offset matching, returning the list of the best-scoring matches
 * for token found in the list of candidates
 */
fun getFuzzyMatches(token: String, candidates: List<String>) =
        fuzzyMatcher.groupedMatch(token, candidates)
                .entries.firstOrNull()
                ?.let { offsetMatcher.groupedMatch(token, it.value) }
                ?.entries?.first()?.value
                ?: emptyList()

infix fun Int.pow(exponent: Int) = Math.pow(this.toDouble(), exponent.toDouble()).toInt()
