package org.tkeating.gi.parser

import org.tkeating.gi.extensions.each
import org.tkeating.gi.org.tkeating.gi.parser.FuzzyContainer

/**
 * Class that wraps a map of strings (tokens) to arbitrary content, with the capability
 * of looking up keys with fuzzy matching
 */
class FuzzyMap(val tokens: Map<String, Any>) : Map<String, Any>, FuzzyContainer {
    override val entries: Set<Map.Entry<String, Any>>
        get() = tokens.entries

    override val keys: Set<String>
        get() = tokens.keys

    override val size: Int
        get() = tokens.size

    override val values: Collection<Any>
        get() = tokens.values

    override fun containsValue(value: Any): Boolean  = tokens.containsValue(value)

    override fun fuzzyContains(element: String) = containsKey(element)

    override fun isEmpty(): Boolean = tokens.isEmpty()

    /**
     * Returns true if the wrapped map contains the requested key, or failing that,
     * if a single fuzzy match exists
     */
    override fun containsKey(key: String): Boolean =
            tokens.containsKey(key) || getFuzzyMatches(key, tokens.keys.toList()).size == 1

    /**
     * Return value by exact match, or if not found, by using fuzzy matching on the key.
     * Throws NotFoundException if no match is found
     * Throws AmbiguousResultException if more than one equally likely candidate exists
     */
    override fun get(key: String)=
            tokens.getOrElse(key) { tokens[getKey(key)] }

    /**
     * Perform fuzzy match on key, but return the fully-matched key rather than the value
     */
    fun getKey(key: String) = fuzzyMatchToken(key, tokens.keys.toList())

    operator fun plus(other: FuzzyMap): FuzzyMap = FuzzyMap(tokens + other.tokens)
}
