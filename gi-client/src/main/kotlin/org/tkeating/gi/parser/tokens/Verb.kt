package org.tkeating.gi.org.tkeating.gi.parser.tokens

import org.tkeating.gi.commands.*
import org.tkeating.gi.extensions.toFuzzyList
import org.tkeating.gi.factories.builders.CreateCommandBuilder
import org.tkeating.gi.factories.builders.ListCommandBuilder
import org.tkeating.gi.factories.builders.RemoveCommandBuilder
import org.tkeating.gi.org.tkeating.gi.parser.tokens.Verb.*

enum class Verb {
    commit,
    complete,
    create,
    fuzztest,
    list,
    pull,
    remove,
    stage,
    switch,
    unstage,
}

fun verbBuilderFactory(verb: Verb, args: List<Any>?) =
        when (verb) {
            commit -> CommitCommand(args)
            complete -> AutocompleteCommand(args)
            create -> CreateCommandBuilder(args)
            fuzztest -> FuzzTestCommand(args)
            list -> ListCommandBuilder(args)
            pull -> PullCommand(args)
            remove -> RemoveCommandBuilder(args)
            stage -> StageCommand(args)
            switch -> SwitchCommand(args)
            unstage -> UnstageCommand(args)
        }

// Verbs that take a direct object
val verbList = listOf(create, list, remove)
        .map(Verb::name)
        .toFuzzyList()

val simpleVerbList = listOf(
        commit,
        complete,
        fuzztest,
        pull,
        stage,
        switch,
        unstage
        ).map(Verb::name)
        .toFuzzyList()
