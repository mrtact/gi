package org.tkeating.gi.parser

import org.tkeating.gi.extensions.toFuzzyMap

interface Modifier;

sealed class DirectObject : Token {
    val modifiers = mutableListOf<Modifier>()
}

object Branches : DirectObject()

object Changes : DirectObject()

object Commits : DirectObject()

object Remotes : DirectObject()

object Stashes : DirectObject()

object Tags : DirectObject()

object Users : DirectObject()

val directObjects = mapOf(
        "branches" to Branches,
        "changes" to Changes,
        "commits" to Commits,
        "remotes" to Remotes,
        "stashes" to Stashes,
        "users" to Users,
        "tags" to Tags
).toFuzzyMap()
