package org.tkeating.gi.parser

import org.tkeating.gi.extensions.each
import org.tkeating.gi.org.tkeating.gi.parser.FuzzyContainer

/**
 * Class that wraps a map of strings (tokens) to arbitrary content, with the capability
 * of looking up keys with fuzzy matching
 */
class FuzzyList(val tokens: List<String>) : List<String>, FuzzyContainer {
    override fun containsAll(elements: Collection<String>): Boolean = tokens.containsAll(elements)

    override fun get(index: Int): String = tokens.get(index)

    override fun indexOf(element: String): Int = tokens.indexOf(element)

    override fun iterator(): Iterator<String> = tokens.iterator()

    override fun lastIndexOf(element: String): Int = tokens.lastIndexOf(element)

    override fun listIterator(): ListIterator<String> = tokens.listIterator()

    override fun listIterator(index: Int): ListIterator<String> = tokens.listIterator(index)

    override fun subList(fromIndex: Int, toIndex: Int): List<String> = tokens.subList(fromIndex, toIndex)

    override val size: Int = tokens.size

    override fun contains(element: String): Boolean  = tokens.contains(element)

    override fun isEmpty(): Boolean = tokens.isEmpty()

    /**
     * Returns true if the wrapped map contains the requested key, or failing that,
     * if a single fuzzy match exists
     */
    override fun fuzzyContains(element: String): Boolean =
            tokens.contains(element) || getFuzzyMatches(element, tokens).size == 1

    fun fuzzyGet(element: String) = getFuzzyMatches(element, tokens)[0]

    operator fun plus(other: FuzzyList): FuzzyList = FuzzyList(tokens + other.tokens)
}
