package org.tkeating.gi.git

import org.tkeating.gi.Gi

object GitConfig {
    val editor: String
        get() = Gi.repo.config.getString("core", null, "editor") ?:
                    System.getenv("GIT_EDITOR") ?:
                    "vim"

    val sshKeyName: String?
        get() = Gi.repo.config.getString("gi",
                null, "sshKeyName")
}
