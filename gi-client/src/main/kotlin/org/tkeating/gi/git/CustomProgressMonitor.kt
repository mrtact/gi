package org.tkeating.gi.org.tkeating.gi.git

import org.eclipse.jgit.lib.ProgressMonitor

class CustomProgressMonitor : ProgressMonitor {
    override fun update(completed: Int) {
        println("Updated $completed")
    }

    override fun start(totalTasks: Int) {
        println("Started $totalTasks")
    }

    override fun beginTask(title: String?, totalWork: Int) {
        println("Begin task $title $totalWork")
    }

    override fun endTask() {
        println("End task")
    }

    override fun isCancelled(): Boolean {
        return false
    }
}
