package org.tkeating.gi.git

import org.eclipse.jgit.api.AddCommand
import org.eclipse.jgit.api.ResetCommand
import org.eclipse.jgit.api.RmCommand
import org.eclipse.jgit.lib.BranchTrackingStatus
import org.eclipse.jgit.lib.Repository
import org.tkeating.gi.extensions.each
import java.io.File
import java.nio.file.Paths

fun AddCommand.addAll(adds: Collection<String>): AddCommand {
    adds.each(this::addFilepattern)
    return this
}

fun RmCommand.addAll(removes: Collection<String>): RmCommand {
    removes.each(this::addFilepattern)
    return this
}

fun ResetCommand.addAll(resets: Collection<String>): ResetCommand {
    resets.each(this::addPath)
    return this
}

/**
 * Given a repository resource at repoPath, compute the relative path to it
 * from workingPath
 */
fun Repository.relativePath(workingPath: File, repoPath: String): String {
    val fullPath = Paths.get(workTree.canonicalPath, repoPath)
    val relPath = workingPath.toPath().relativize(fullPath).toString()
    return relPath
}

/**
 * Format a branch's tracking status for inclusion in the header of the
 * List Changes output
 */
fun BranchTrackingStatus?.remoteInfo() =
        this?.let { status ->
            val ahead = if (status.aheadCount != 0) "+${status.aheadCount}" else "--"
            val behind = if (status.behindCount != 0) "-${status.behindCount}" else "--"
            "[${Repository.shortenRefName(status.remoteTrackingBranch)} $ahead/$behind]"
        } ?: "[No remote branch]"
