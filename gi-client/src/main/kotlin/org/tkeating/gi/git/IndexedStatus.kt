package org.tkeating.gi.org.tkeating.gi.git

import org.eclipse.jgit.api.Status
import java.io.File

typealias StatusIndex = MutableMap<String, MutableList<String>>

/**
 * Wrapper around a JGit Status object which constructs a bunch of indices, letting us quickly search
 * for matches by full or partial path (filename)
 */
class IndexedStatus(private val status: Status, private val nameOnly: Boolean = false) {
    val addedIndex: StatusIndex
    val changedIndex: StatusIndex
    val removedIndex: StatusIndex
    val missingIndex: StatusIndex
    val modifiedIndex: StatusIndex
    val untrackedIndex: StatusIndex
    val conflictingIndex: StatusIndex

    val stagedIndex: StatusIndex
    val unstagedIndex: StatusIndex

    init {
        addedIndex = buildIndex(status.added)
        changedIndex = buildIndex(status.changed)
        removedIndex = buildIndex(status.removed)
        missingIndex = buildIndex(status.missing)
        modifiedIndex = buildIndex(status.modified)
        untrackedIndex = buildIndex(status.untracked)
        conflictingIndex = buildIndex(status.conflicting)

        stagedIndex = (addedIndex + removedIndex + changedIndex) as StatusIndex
        unstagedIndex = (missingIndex + modifiedIndex + untrackedIndex) as StatusIndex
    }

    private fun buildIndex(keys: Set<String>): StatusIndex {
        val index = mutableMapOf<String, MutableList<String>>()
        keys.forEach {path ->
            val name = path.substringAfterLast(File.separatorChar)
            index.addKeyVal(name, path)

            if (!nameOnly) {
                recursivePathGenerator(path)
                        .forEach { index.addKeyVal(it, path) }
            }
        }
        return index
    }
}

fun recursivePathGenerator(path: String) =
    generateSequence(
            seedFunction = { if (path.contains(File.separatorChar)) path else "" },
            nextFunction = {
                if (it.contains(File.separatorChar)) it.substringBeforeLast(File.separatorChar) else null
            }
    )

fun StatusIndex.addKeyVal(key: String, value: String) {
    val list = getOrDefault(key, mutableListOf())
    list += value
    this[key] = list
}
