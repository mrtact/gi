package org.tkeating.gi

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Repository

object Gi {
    lateinit var getGit: () -> Git

    val git: Git by lazy {
        getGit()
    }

    val repo: Repository by lazy {
        git.repository
    }
}
