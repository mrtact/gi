package org.tkeating.gi.commands

import org.tkeating.gi.Gi
import org.tkeating.gi.extensions.*
import org.tkeating.gi.commands.list.ListChanges
import org.tkeating.gi.git.addAll
import org.tkeating.gi.org.tkeating.gi.commands.exception.AmbiguousArgumentException
import org.tkeating.gi.org.tkeating.gi.git.IndexedStatus
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.Params

/**
 * Stage/unstage specified changes. The default argument is ALL.
 */
private const val REMOVE = "remove"
private const val ADD = "add"
private const val RESET = "reset"
private const val NONE = "none"

class StageCommand(args: List<Any>? = null) : CommandChain(args) {
    override val valid: Boolean
        get() = true

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() =
            if (args?.isEmpty() == true) {
                doStage(allFilesOf(unstagedBlock))
            } else {
                doStage(args.toStringList())
            }

    override fun complete(): List<String> =
            IndexedStatus(Gi.git.status().call(), nameOnly = true)
                    .unstagedIndex.keys.toList()
                    .matchingLastParam(args)

    fun doStage(argList: List<String>) {
        val status = IndexedStatus(Gi.git.status().call())

        argList.map { arg ->

            // If a short arg (filename) matches more than one file, die
            status.stagedIndex[arg]?.let {
                if (it.size > 1) {
                    throw AmbiguousArgumentException(arg, it)
                }
            }

            status.run {
                when (arg) {
                    in missingIndex -> REMOVE to missingIndex[arg]!!
                    in modifiedIndex -> ADD to modifiedIndex[arg]!!
                    in untrackedIndex -> ADD to untrackedIndex[arg]!!
                    else -> NONE to listOf(arg)
                }
            }
        }
                .groupBy({ it.first }, { it.second })
                .forEach { (command, files) ->
                    files.flatMap { it }
                            .also {
                                when (command) {
                                    ADD -> Gi.git.add().addAll(it).call()
                                    REMOVE -> Gi.git.rm().addAll(it).call()
                                    NONE -> it.each { println("Could not stage $it") }
                                }
                            }
                }

        ListChanges().execute()
    }
}

class UnstageCommand(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = true

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() =
            if (args?.isEmpty() == true) {
                doUnstage(allFilesOf(stagedBlock))
            } else {
                doUnstage(args?.map(Any::toString) ?: emptyList())
            }

    override fun complete(): List<String> =
            IndexedStatus(Gi.git.status().call(), nameOnly = true)
                    .stagedIndex.keys.toList()
                    .matchingLastParam(args)

    fun doUnstage(argList: List<String>) {
        val status = IndexedStatus(Gi.git.status().call())

        argList.map { arg ->

            // If a short arg (filename) matches more than one file, die
            status.unstagedIndex[arg]?.let {
                if (it.size > 1) {
                    throw AmbiguousArgumentException(arg, it)
                }
            }

            status.run {
                when (arg) {
                    in addedIndex -> RESET to addedIndex[arg]!!
                    in removedIndex -> RESET to removedIndex[arg]!!
                    in changedIndex -> RESET to changedIndex[arg]!!
                    else -> NONE to listOf(arg)
                }
            }
        }
                .groupBy({ it.first }, { it.second })
                .forEach { (command, files) ->
                    files.flatMap { it }
                            .also {

                                when (command) {
                                    RESET -> Gi.git.reset().addAll(it).call()
                                    NONE -> files.each { println("Could not unstage $it") }
                                }
                            }
                }

        ListChanges().execute()
    }
}

private fun allFilesOf(fstatList: List<FileStatus>): List<String> {
    val status = Gi.git.status().call()

    // Iterate the staged file statuses, map them to the appropriate sets of
    // filenames from git status, and then flatten them into a command
    return fstatList.map { fstat ->
        status.byStatus(fstat).map { it }
    }.flatMap { it }
}
