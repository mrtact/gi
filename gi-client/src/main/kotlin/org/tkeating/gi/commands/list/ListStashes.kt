package org.tkeating.gi.factories.command

import org.tkeating.gi.Gi
import org.tkeating.gi.commands.CommandWrapper
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.Params
import org.tkeating.gi.renderers.DefaultRevsRenderer

class ListStashes(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = true

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() =
            println(DefaultRevsRenderer.render(Gi.git.stashList().call()))
}
