package org.tkeating.gi.commands.remove

import org.eclipse.jgit.lib.Repository
import org.tkeating.gi.Gi
import org.tkeating.gi.commands.CommandWrapper
import org.tkeating.gi.extensions.toStringList
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.Params
import org.tkeating.gi.renderers.DefaultRemoveRenderer

class RemoveBranch(args: List<Any>?) : CommandChain(args) {

    override val valid
        get() = args?.isNotEmpty() == true && args.all { it is String }

    override val help = "gi remove branch [names...]"

    override fun execute() {
        val resultSet = with(Gi.git.branchDelete()) {
            setBranchNames(*args!!.toStringList().toTypedArray())
        }
                .call()
                .map(Repository::shortenRefName)
                .toSet()

        println(DefaultRemoveRenderer(Params.empty, resultSet).render())
    }
}
