package org.tkeating.gi.commands.create

import org.tkeating.gi.Gi
import org.tkeating.gi.commands.CommandWrapper
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.Params

class CreateBranch(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = args?.size == 1 && args.get(0) is String

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() {
        Gi.git.branchCreate().setName(args!!.get(0).toString()).call()
    }
}
