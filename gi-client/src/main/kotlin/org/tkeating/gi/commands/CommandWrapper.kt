package org.tkeating.gi.commands

import org.tkeating.gi.parser.Params

interface CommandWrapper {
    val help: String
        get() = ""

    fun valid(params: Params) = true

    fun execute(params: Params) = if (valid(params)) {
        executeLocal(params)
    } else {
        println(help)
    }

    fun executeLocal(params: Params)

    fun complete(params: Params) = listOf("")
}
