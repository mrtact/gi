package org.tkeating.gi.commands

import org.eclipse.jgit.lib.Repository
import org.tkeating.gi.Gi
import org.tkeating.gi.extensions.matchingLastParam
import org.tkeating.gi.commands.list.ListChanges
import org.tkeating.gi.org.tkeating.gi.extensions.isBranch
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.org.tkeating.gi.util.countdown
import org.tkeating.gi.parser.Params

class SwitchCommand(args: List<Any>?) : CommandChain(args) {

    override val valid: Boolean
            get() = args?.size == 1

    override val help = "gi switch [branch]"

    override fun complete() =
            Gi.git.branchList().call()
                    .map { Repository.shortenRefName(it.name) }
                    .matchingLastParam(args)

    override fun execute() {
        val branchName = args?.get(0)?.toString() ?: ""
        if (!branchName.isBranch) {
            countdown("Branch '$branchName' does not exist", "Creating") { createBranch(branchName) }
        }

        with (Gi.git.checkout()) {
            setName(branchName)
        }.call()

        ListChanges().execute()
    }

    fun createBranch(branchName: String) {
        with (Gi.git.branchCreate()) {
            setName(branchName)
        }.call()
    }
}
