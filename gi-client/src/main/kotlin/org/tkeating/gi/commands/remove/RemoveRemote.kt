package org.tkeating.gi.commands.remove

import org.tkeating.gi.Gi
import org.tkeating.gi.commands.CommandWrapper
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.Params
import org.tkeating.gi.renderers.DefaultRemoveRenderer

class RemoveRemote(args: List<Any>?) : CommandChain(args) {
    override val help = "gi remove remote [names]"

    override val valid
            get() = args?.size == 1 && args[0] is String

    override fun execute() {
        val before = setOfRemoteNames()

        with (Gi.git.remoteRemove()) {
            setName(args!![0].toString())
            this
        }.call()

        val after = setOfRemoteNames()
        val removed = before - after

        println(DefaultRemoveRenderer(Params.empty, removed).render())
    }
}

fun setOfRemoteNames() = Gi.git.remoteList()
        .call()
        .map { it.name }
        .toSet()
