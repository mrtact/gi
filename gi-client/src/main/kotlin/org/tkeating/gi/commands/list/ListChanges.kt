package org.tkeating.gi.commands.list

import org.tkeating.gi.Gi
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.renderers.changes.DefaultChangesTemplate

class ListChanges(args: List<Any>? = null) : CommandChain(args) {
    override val valid: Boolean
        get() = true

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() = Gi.git.status().call().run {
        println(DefaultChangesTemplate.render(this))
    }
}
