package org.tkeating.gi.commands.create

import org.tkeating.gi.Gi
import org.tkeating.gi.commands.CommandWrapper
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.Params

class CreateTag(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = args?.size == 1 &&  args[0] is String

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() {
        with (Gi.git.tag()) {
            name = args!!.get(0).toString()
            setAnnotated(true)
        }.call()
    }
}
