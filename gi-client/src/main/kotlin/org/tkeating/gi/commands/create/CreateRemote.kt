package org.tkeating.gi.commands.create

import org.eclipse.jgit.transport.URIish
import org.tkeating.gi.Gi
import org.tkeating.gi.commands.CommandWrapper
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.parser.Params

class CreateRemote(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = args?.size == 2 && args[0] is String && args[1] is String

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() {
        if (args?.size != 2) {
            println("gi create remote [name] [URI]")
            return
        }

        Gi.git.remoteAdd().apply {
            setName(args[0].toString())
            setUri(URIish(args[1].toString()))
        }.call()
    }
}
