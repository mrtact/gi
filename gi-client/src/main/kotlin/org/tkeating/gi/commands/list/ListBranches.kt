package org.tkeating.gi.commands.list

import org.tkeating.gi.Gi
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.renderers.DefaultRefsRenderer

class ListBranches(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = true

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() = println(DefaultRefsRenderer.render(Gi.git.branchList().call()))
}
