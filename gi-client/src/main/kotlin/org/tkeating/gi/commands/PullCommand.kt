package org.tkeating.gi.commands

import org.tkeating.gi.Gi
import org.tkeating.gi.org.tkeating.gi.git.CustomProgressMonitor
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.org.tkeating.gi.util.getTransportConfigCallback

class PullCommand(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = true

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() {
        val isDirty = !Gi.git.status().call().isClean

        if (isDirty) {
            Gi.git.stashCreate().call()
        }

        Gi.git.pull().apply() {
            remote = "origin"
            setProgressMonitor(CustomProgressMonitor())
            setTransportConfigCallback(getTransportConfigCallback())
        }.call()

        println()
        if (isDirty) {
            Gi.git.stashApply().call()
            Gi.git.stashDrop().call()
        }
    }
}
