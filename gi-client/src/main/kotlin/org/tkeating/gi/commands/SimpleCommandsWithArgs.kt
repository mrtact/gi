package org.tkeating.gi.commands

import org.tkeating.gi.Gi
import org.tkeating.gi.extensions.hasStaged
import org.tkeating.gi.factories.builders.parseCommand
import org.tkeating.gi.git.GitConfig
import org.tkeating.gi.org.tkeating.gi.parser.CommandChain
import org.tkeating.gi.org.tkeating.gi.parser.tokens.verbList
import org.tkeating.gi.org.tkeating.gi.parser.tokens.simpleVerbList
import org.tkeating.gi.parser.directObjects
import org.tkeating.gi.parser.getFuzzyMatches
import java.io.File

class CommitCommand(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = true

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() {
        val status = Gi.git.status().call()
        if (!status.hasStaged()) {
            println("Nothing to commit")
            return
        }

        val commitMsg = if (args?.isNotEmpty() == true) {
            args.joinToString(" ")
        } else {
            spawnEditorForCommitMessage()
        }

        if (commitMsg.isNotBlank()) {
            Gi.git.commit().setMessage(commitMsg).call()
        } else {
            println("Empty commit message, aborting")
        }
    }

    fun spawnEditorForCommitMessage(): String {
        val tempFile = File.createTempFile("gitcommit", ".tmp")
        tempFile.deleteOnExit()

        val commandLine = (GitConfig.editor.split(Regex("""\s+""")) + tempFile.absolutePath).toTypedArray()

        val editor = ProcessBuilder().command(*commandLine).start()
        editor.waitFor()

        return tempFile.readText()
    }
}

class FuzzTestCommand(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = true

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() {
        val token = args?.get(0).toString()
        val candidates = verbList + simpleVerbList + directObjects.keys
        println(getFuzzyMatches(token, candidates))
    }
}

class AutocompleteCommand(args: List<Any>?) : CommandChain(args) {
    override val valid: Boolean
        get() = true

    override val help: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun execute() =
            // We strip off the first param because it should be `gi`
            parseCommand(args?.drop(1)?.map(Any::toString) ?: emptyList())
                    .complete()
                    .sorted().forEach(::println)
}
