package org.tkeating.gi.org.tkeating.gi.commands.exception

class AmbiguousArgumentException(override val message: String?)
    : Exception(message) {

    constructor(arg: String, candidates: List<String>) : this(format(arg, candidates))

    companion object {
        fun format(arg: String, candidates: List<String>): String {
            val first = "Ambiguous argument $arg. Could be one of\n\t"
            return candidates.joinToString("\n\t", prefix=first)
        }
    }
}
