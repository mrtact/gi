package org.tkeating.gi

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.errors.TransportException
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.tkeating.gi.factories.builders.ParseError
import org.tkeating.gi.factories.builders.parseCommand
import org.tkeating.gi.org.tkeating.gi.commands.exception.AmbiguousArgumentException


fun main(args: Array<String>) {
    Gi.getGit = ::initGit
    runMain(args.toList())
}

fun initGit() = Git(FileRepositoryBuilder()
        .findGitDir()
        .build())

fun runMain(args: List<String>) {
    try {
        parseCommand(args).execute()
    } catch (e: ParseError) {
        println(e.message)
    } catch (e: IllegalArgumentException) {
        println("Not a git repository")
    } catch (e: AmbiguousArgumentException) {
        println(e.message)
    } catch (e: TransportException) {
        if (e.message == "USERAUTH fail") {
            println("Invalid password for your SSH key")
        } else {
            throw e
        }
    }
}
